package ru.t1.kravtsov.tm.exception.system;

public final class AccessDeniedException extends AbstractSystemException {

    public AccessDeniedException() {
        super("Error. Access denied.");
    }

}
