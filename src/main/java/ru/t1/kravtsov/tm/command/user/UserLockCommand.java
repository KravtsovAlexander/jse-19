package ru.t1.kravtsov.tm.command.user;

import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Lock user.";

    public static final String NAME = "user-lock";

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
