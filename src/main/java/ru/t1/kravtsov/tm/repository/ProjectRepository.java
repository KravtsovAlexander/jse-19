package ru.t1.kravtsov.tm.repository;

import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.model.Project;

import java.util.List;
import java.util.stream.Collectors;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository() {
    }

    @Override
    public List<Project> removeByName(final String userId, final String name) {
        return findAll(userId)
                .stream()
                .filter(m -> name.equals(m.getName()))
                .peek(m -> models.remove(m.getId()))
                .collect(Collectors.toList());
    }

}
